const sortBy = require('lodash/sortBy');

const resource = {
    path: '/user/info/data',
    permissions: [
        { user: 'matt', isActive: true, type: 'WRITE' },
        { user: 'matt', isActive: false, type: 'DELETE' },
        { user: 'darren', isActive: true, type: 'WRITE' },
        { user: 'darren', isActive: true, type: 'CREATE' },
        { user: 'terry', isActive: true, type: 'READ' },
        { user: 'joe', isActive: true, type: 'READ' },
        { user: 'terry', isActive: true, type: 'DELETE' },
        { user: 'terry', isActive: true, type: 'CREATE' },
        { user: 'matt', isActive: true, type: 'CREATE' },
    ]
};

// Easy to confuse variable names due to large lexical context
//    permissions.type instead of permission.type
// Copying longer code causes likelihood of missing things
//    forget to change list.includes to deleteList.includes
// Easy to not rename things when context changes
//    change list to createOrWriteList
// Logic get spread across code
//    sorting is done later after list is made
// Incremental changes rather than refactoring
//    not creating a separate function to find permission list

const resourcePermissionSummary = permissions => {
    const list = [];

    permissions.forEach(permission => {
        if (permission.isActive && ['CREATE', 'WRITE'].includes(permissions.type)) {
            if (!list.includes(permission.user)) {
                list.push(permission.user);
            }
        }
    });

    const deleteList = [];

    permissions.forEach(permission => {
        if (permission.isActive && 'DELETE' === permission.type) {
            if (!list.includes(permission.user)) {
                deleteList.push(permission.user);
            }
        }
    });

  return {
      createOrWrite: sortBy(list),
      delete: sortBy(deleteList),
  }
};

console.log(resourcePermissionSummary(resource.permissions));
