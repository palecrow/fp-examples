# Comparing procedural and functional programming approaches

## Overview

I've found that I see a lot of procedural code when people write UIs, and this is
probably because this is what traditional object-oriented training prepares people for.
I find that procedural code, however, tends to lead to crufty, 'shaggy-dog' style code
that becomes meandering, complex to manage, and downright buggy.  In this document I
will show a typical scenario that I see when programmers iterate on procedural code, and
then contrast that with how the scenario would develop had the programmer used FP
approaches.

Some of the coding pitfalls demonstrated here could be attributed to a kind of laziness of process
(such as not using a function to do similar things) but again, this is the kind of thing
that I believe procedural code leads to.  I call procedural code 'laundry list' code
(although that sounds pleasantly declarative); what I mean by that moniker is that
there's a kind of haphazard, meandering, "do-this, then I guess do that, oh wait, let me put this
other thing in here somewhere before that" feel to most procedural code.  FP code on the
other hand is more transformative, only presenting declared values when needed and
otherwise just creating paths by which data is transformed.

## Use case

The initial use case is:

  * Given a particular `resource` structure,
  * Write a function which returns an object with a property `createOrWrite`,
  * That lists the names of those who have active `CREATE` or `WRITE` permissions to that object.


The structure of the data is:

```
const resource = {
    path: '/user/info/data',
    permissions: [
        { user: 'matt', isActive: true, type: 'WRITE' },
        { user: 'matt', isActive: false, type: 'DELETE' },
        { user: 'darren', isActive: true, type: 'WRITE' },
        { user: 'darren', isActive: true, type: 'CREATE' },
        { user: 'terry', isActive: true, type: 'READ' },
        { user: 'joe', isActive: true, type: 'READ' },
        { user: 'terry', isActive: true, type: 'DELETE' },
        { user: 'terry', isActive: true, type: 'CREATE' },
        { user: 'matt', isActive: true, type: 'CREATE' },
    ]
};
```

The expectation is that in calling this function with `resource.permissions` you'd get an object like:

```
{ createOrWrite: ['matt', 'darren', 'terry'] }
```

The first attempt at solving this is a simple procedural function.  It's
considered procedural because it procedurally goes through the permission
list :

```
const resourcePermissionSummary = permissions => {
    const list = [];

    permissions.forEach(permission => {
        if (permission.isActive && ['CREATE', 'WRITE'].includes(permissions.type)) {
            list.push(permission.user);
        }
    });


    return {
        createOrWrite: list,
    }
};
```

This looks pretty good, until you realize that it doesn't quite work.  It yields:

```
{ createOrWrite: [] }
```

You realize after a little bit of analysis that that's because you meant:

```
.includes(permission.type)) {
```

You fix that and continue.  You now get: `{ createOrWrite: [ 'matt', 'darren', 'terry', 'matt' ] }`

You realize 'matt' is repeated because he has both a `CREATE` and a `WRITE` permission.
You figure that you should not push when you already have the user in the list, so you modify
the function so that it now reads the following:

```
const resourcePermissionSummary = permissions => {
    const list = [];

    permissions.forEach(permission => {
        if (permission.isActive && ['CREATE', 'WRITE'].includes(permissions.type)) {
            if (!list.includes(permission.user)) {
                list.push(permission.user);
            }
        }
    });


    return {
        createOrWrite: list,
    }
};
```

This works as expected, yielding `{ createOrWrite: [ 'matt', 'darren', 'terry' ] }`.  All looks good.
You realize you could have also just put the conditional in with the other conditionals on the line above
but you wanted to reduce the lines of code that would change.

Your business analyst looks at the result and asks you to sort the names alphabetically, so you just do this
at the last moment in the return:

```
    return {
        createOrWrite: sortBy(list),
    }
```

Now you get: `{ createOrWrite: [ 'darren', 'matt', 'terry'] }` and everyone is happy for a while.

Eventually your business analyst says that we need another property on the return object: `delete`, which
contains the list of users with active `DELETE` permissions on the object (ordered and unique).  You could
break out a separate function, but you instead figure it's easy enough to just copy the code from above down
below and assign the result to a different array.  You end up with:

```
const resourcePermissionSummary = permissions => {
    const list = [];

    permissions.forEach(permission => {
        if (permission.isActive && ['CREATE', 'WRITE'].includes(permissions.type)) {
            if (!list.includes(permission.user)) {
                list.push(permission.user);
            }
        }
    });

    const deleteList = [];

    permissions.forEach(permission => {
        if (permission.isActive && 'DELETE' === permission.type) {
            if (!list.includes(permission.user)) {
                deleteList.push(permission.user);
            }
        }
    });

  return {
      createOrWrite: sortBy(list),
      delete: sortBy(deleteList),
  }
};
```

You run it and get the result:

```
{
    createOrWrite: [ 'darren', 'matt', 'terry'],
    delete: []
}
```

This doesn't seem right, it seems like `terry` should be listed in the `delete` array.
You notice the reason; in copying the code, you were checking the
unique exclusion against the old `list` rather than `deleteList`.  You fix that bug and are
now done (see `proceduralPermissions.js`).

Not so bad, right?  I mean, you can look at the code and everything works.  You had a couple of
bugs along the way, but your manager is happy you are done with your task.  You slap your hands
together and move on.

## Analysis of Procedural approach

There are several observations I have about what transpired to write `proceduralPermissions.js`.

  * It was easy to confuse variable names due to large lexical context (it's not even that large).
    We mistakenly typed `permissions.type` instead of `permission.type` because both
    `permissions` and `permission` existed within the same scope.
  * Copying longer code causes likelihood of missing things.
    In this case we forgot to change `list.includes` to `deleteList.includes` in the copied code.
  * It's easy to not rename things when context changes.
    Here, we kept the old name `list` and added `deleteList` rather than renaming `list` to
    `createOrWriteList`, which might have made it easy to see where we went wrong down below.
  * Logic got spread across code.
    In this code, sorting is done later after list is made, which is fine, but it may not be
    apparently evident.  If more code/requirements are added, these transformations may become
    even less obvious.
  * We made incremental changes rather than refactoring.
    We didn't make the effort to create a separate function to find the permission list.

Each of these contributed to simple bugs that were, luckily, easy to find.  But they still
came up, and weren't completely obvious.  What if the code became more complex?

## FP approach

Let's imagine the same scenario, but using an FP approach.  We'll take the code through the same
series of changes as we originally had.  At first, we want a function that takes the resource
permissions and returns an object with `createOrWrite` as an array of user names where there
are active `CREATE` and `WRITE` permissions.

FP would say that we tackle the atomic elements as functions.  Roughly, at the outside,
we want a function that looks something like:

```
const fpResourcePermissionSummary = permissions => ({
    createOrWrite: users(permissions),
});
```

For this we need to construct a function `users` that, given permissions, constructs the array.
Typically if we want to transform something, especially using a series of steps, we start with
lodash's `compose` function.  The first execution of compose takes in arguments of transformations,
from last to first.  The second execution takes in the value which then is passed into the last
argument, and results are chained back to the previous argument, etc.

```
const users = permissions =>
    compose(
        ...third-to-last function takes the result of the second-to-last function
        ...second-to-last function takes the result of the next function
        ...last listed function takes `permissions` in as a single arg to its input
    )(permissions);
```

In this case we are getting an array of permissions, and we want to filter both by the fact
that the permission is active, and that it's in the list we want.  We could write code like:

```
const isWhatWeWant = permission => permission.isActive && ['CREATE', 'WRITE'].includes(permission.type);

const users = permissions =>
  compose(
      map('user'),
      filter(isWhatWeWant),
  )(permissions);
```

This seems like a lot of cruft for the short term.  But it does work, except for the uniqueness, and the sorting.
This is where compose gets simple (you can probably even simplify further, but I'm just demonstrating `compose`):

```
const users = permissions =>
  compose(
      sortBy,
      uniq,
      map('user'),
      filter(isWhatWeWant),
  )(permissions);
```

`sortBy` and `uniq` are lodash functions that can take in a single argument, so the result of
`filter(isWhatWeWant)` is passed back to `map` which transforms the list of filtered permissions
into an array of user strings; this is passed to `uniq` and then its result is passed back to `sortBy`.

Now as much as this works, I think it's a little vague to understand what 'isWhatWeWant' is, and the code seems
really specific.  Here's what I'm going to suggest instead: let's break down the conditions.

```
const isActive = prop('isActive');
const createOrWrite = permission => ['WRITE', 'CREATE'].includes(permission.type));

const users = permissions =>
  compose(
      sortBy,
      uniq,
      map('user'),
      filter(allPass([
          isActive,
          createOrWrite,
      ])),
  )(permissions);
```

This introduces a few new things.  `allPass()` is a function that takes in an array of functions,
and given an input, it runs the functions in the array using the input, and if all of those
functions return truthy, it returns `true`.  That means that we will only find permissions that have
the property `isActive` truthy, and `type` of either `CREATE` or `WRITE`.

`isActive` could also just be written `const isActive = permission => permission.isActive;` but we use the
`prop` function to make this less verbose.  You could also just place `prop('isActive')` in the `allPass`
function argument, although it seems the 'isActive' function could be useful elsewhere.

This is all working pretty well, until we get the requirement for returning the `delete` property as well,
as above.  So we need to replace `createOrWrite` with something a little more dynamic.  Let's put
all the code together now:

```
const isActive = prop('isActive');
const anyOfTypes = curry((types, permission) => types.includes(permission.type));

const usersByTypes = (types, permissions) => compose(
    sortBy,
    uniq,
    map('user'),
    filter(allPass([
        isActive,
        anyOfTypes(types),
    ])),
)(permissions);

const fpResourcePermissionSummary = permissions => ({
    createOrWrite: usersByTypes(['CREATE', 'WRITE'], permissions),
    delete: usersByTypes(['DELETE'], permissions),
});
```

So we did a few things.  We changed `users` to `usersByTypes` because we wanted to specify the types
of permissions we're looking for.  So `types` was added as an argument; this was now passed into `anyOfTypes`
which replaced our old `createOrWrite`.

`anyOfTypes` includes a concept we haven't talked about: currying.  _This is Not The Only Way To Do It (tm)_ by
any means.  In this case, we could have just written:

```
const anyOfTypes = types => permission => types.includes(permission.type);
```

But currying is a way of keeping our future options open, because it allows us to call `anyOfTypes` as
either of the following:

```
anyOfTypes(['CREATE', 'WRITE'], permission);
anyOfTypes(['CREATE', 'WRITE'])(permission);
```

It's this last form that works well in a `compose` environment, because it accepts a single argument, in
this case the permission evaluated by `allPass` as provided by `filter`.

The result is actually pretty legible.  Let's narrate the entire code:

  * `fpResourcePermissionsSummary` takes in an array of permissions and produces an object with two props:
    `createOrWrite` and `delete`.  Both call `usersByTypes` with the types needed, and the permission list.
  * `usersByTypes` in turn takes the permissions, and first filters where the permission is both:
    * active
    * is any of the given types

    The remaining permissions then get their names extracted, this list of names is stripped for uniqueness,
    and then sorted (alphabetically).

That's not too hard to understand!

Almost each of the shortcomings of the procedural code and garden-path errors are not likely to be run into
using FP in this case because, among other things, the lexical scope of any given function is either very
small or nonexistent.  This makes it hard to make referential mistakes.

## Declarative: one more stretch

OK, if that wasn't enough, we can actually go one more step if we want to.  Notice how in `fpResourcePermissionSummary`
the code looks kind of repeated from one prop entry to the next?  We could always use a declarative approach,
so instead of `fpResourcePermissionSummary` we have the following:

```
const summaryMap = {
    createOrWrite: ['CREATE', 'WRITE'],
    delete: ['DELETE'],
};

const declarativeResourcePermissionSummary = (permissions, mapping) =>
  mapValues(x => usersByTypes(x, permissions))(mapping);

console.log(declarativeResourcePermissionSummary(resource.permissions, summaryMap));
```

In this case, we are using `mapValues` to transform the static `summaryMap` into the result we
want given the `resource.permissions` input.  This makes it even easier when the business analyst
comes back and says 'can we have a `read` property to list those who have `READ` access?'  You can
just say 'sure, let me add one line: `read: ['READ'],`...OK, done.'

## Conclusion

Although it is certainly a matter of taste, you can see how FP concepts helped guide us to a solution
that was not only elegant, but felt as though its more atomic components could serve other purposes as well.  By thinking of
creating functions first, we made it easier to adapt to changing requirements.  The 'shaggy-dog'
approach of just adding more explicit procedures to handle more use cases seemed like it could
cause the code to balloon and become rather disjointed, whereas it seemed that moving even beyond
FP into adopting a declarative approach in fact made further use cases easier to adopt.

I have benefited much from using these FP approaches in my codebases, and I hope you do as well.
