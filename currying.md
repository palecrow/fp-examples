# Currying, Partial Application of Functions, and Nested Functions

In functional programming, people talk about currying a lot, but I wanted to be completely clear
about similar concepts that are actually somewhat different.  They are:

* Nested Functions
* Partial Application of Functions
* Curries

## What is in common

Each of these approaches produce cases of functions that return functions.  These are really useful
when composing functions, or when you have to create a function that will later require further input
for execution, such as an action that will be sent to a dispatcher for redux.

But these approaches are not all equal!  They may both be written differently, and behave differently.
Let's just start with some definitions.

## Nested Functions

Nested functions are just functions that return other functions, explicitly.  For example:

```
const adder = x => y => x + y;

const addTwo = adder(2);

addTwo(4);
// 6
```

This is just an explicit return of a function from a function, containing a closure that affects the
returned function's execution.

## Partial Application of Functions

Partial application of a function is a case where you have an existing function with a given arity
and you create a new function that splits the arity.  For example:

```
const superheroNamer = (prefix, delimiter, suffix) => `${prefix}${delimiter}${suffix}`;

const prefixSetter = (prefix, delimiter) => (suffix) => superheroNamer(prefix, delimiter, suffix);

const antNamer = prefixSetter('ant', '-');

antNamer('man');
// 'ant-man'

antNamer('master');
// 'ant-master'
```

You can use tools like lodash's `partial` to do this kind of thing as well:

```
const partial = require('lodash/partial');

const superheroNamer = (prefix, delimiter, suffix) => `${prefix}${delimiter}${suffix}`;

const antNamer = partial(superheroNamer, 'ant', '-');

antNamer('beast');
// 'ant-beast'

antNamer('person');
// 'ant-person'
```

There's actually a little bit more that you can do with partially applied functions depending
on the implementation.  For example, lodash allows you to use placeholders as an advanced feature
of the partial application.

## Currying

Currying goes one step further than the previous two.  In both cases of nested and partially applied functions,
the resulting function's arity was fixed.  Curries are different
because the resulting functions instead have a variable-arity that returns a function containing any remaining arity,
until all are expended.  In other words:

Given a curry _cf_ of function _f_(a, b, c):

_f_(a, b, c) = _cf_(a)(b)(c) = _cf_(a, b)(c) = _cf_(a, b, c) = _cf_(a)(b, c)

For example:

```
const curry = require('lodash/curry');

const superheroNamer = (prefix, delimiter, suffix) => `${prefix}${delimiter}${suffix}`;

const curriedNamer = curry(superheroNamer);

curriedNamer('spider')('')('man');
// 'spiderman'

curriedNamer('spider', '')('man');
// 'spiderman'

curriedNamer('spider')('', 'man');
// 'spiderman'

curriedNamer('spider', '', 'man');
// 'spiderman'
```

Why is this valuable?  Well, let's turn our attention back to `compose` for a minute.  We are
using `compose` a lot to stack transformations of data from an input to an eventual output.  To
make this possible, each given function must take only one argument (so it can consume the result of the
previous function).  Here's an example:

```
const curry = require('lodash/curry');
const compose = require('lodash/fp/compose');
const toUpper = require('lodash/toUpper');

const superheroNamer = (prefix, delimiter, suffix) => `${prefix}${delimiter}${suffix}`;
const curriedNamer = curry(superheroNamer);

const superheroExclaimer = compose(
  name => `It's ${name}!!!!`,
  toUpper,
  curriedNamer('super', '-'), // leaving one remaining argument
);

superheroExclaimer('woman');
// 'It\'s SUPER-WOMAN!!!!'

curriedNamer('cat', '+', 'mouse');
// 'cat+mouse'
```

In other words, you can use the same function in multiple places with different arities, so you don't
have to redefine multiple partial applications.  In these cases it's typical to leave the 'data' argument
as the right-most so it's easy to use in compositions.

lodash also allows for `curryRight` for cases where someone has written a useful function, like
lodash `map` (non-fp) and you want to use it variably:

```
const map = require('lodash/map');
const curryRight = require('lodash/curryRight');
const compose = require('lodash/fp/compose');

map([1,2,3], x => x * 2);
// [ 2, 4, 6 ]

const curriedMap = curryRight(map);

curriedMap([1, 2, 3], x => x * 2);
// [ 2, 4, 6 ]

const mapDoubler = compose(
  curriedMap(x => x * 2),
);

mapDoubler([1,2,3]);
// [ 2, 4, 6 ]
```

Notice how `curryRight` maintains the original function's arity when called with full arity.  This is due
to how lodash chooses to implement the arity handling.  Given _n_-arity calls, it provides
_n_ right-most remaining arguments in original argument order.  The result is a right-curry of
the remaining arguments.  In other words:

curryRight(_f_(a, b, c)) given (x, y) provides (b, c) and yields _f_(a)

curryRight(_f_(a, b, c)) given (x) provides (c) and yields curryRight(_f_(a, b))

curryRight(_f_(a, b, c)) given (x, y, z) provides (a, b, c) and yields result of _f_(a, b, c)

```
const curryRight = require('lodash/curryRight');

const superheroNamer = (prefix, delimiter, suffix) => `${prefix}${delimiter}${suffix}`;
const rightCurriedNamer = curryRight(superheroNamer);

const catSuffixer = rightCurriedNamer('-', 'cat');
catSuffixer('mega')
// 'mega-cat'

const catSuffixerNoDash = rightCurriedNamer('cat');
catSuffixerNoDash('mega', '*')
// 'mega*cat'

```

Read the documentation for more specific information about these transpositions.

## Conclusion

So what did we learn?  There is a difference between nested functions, partial applications, and currying.
In JavaScript, most partial applications are done programatically, but some libraries like lodash provide
additional capabilities like placeholders which extend what you can do with partial applications.

Curries are much more complex because of their variable arities and returned functions, but are very
useful because they allow one to use a base function in a wider variety of contexts, like lodash's
`compose`.
