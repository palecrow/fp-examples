const fp = require('lodash/fp');
const ld = require('lodash');

const tests = [
  x => x > 3,
  x => x % 2 === 0,
  x => (x.a || {}).c === 'b',
];

console.log(
  fp.overEvery(tests)(null),
  fp.overEvery(tests)(undefined),
  fp.overEvery(tests)('a'),
  fp.overEvery(tests)(2),
  fp.overEvery(tests)(3),
  fp.allPass(tests)(4),
  fp.allPass(tests)(5),
  fp.overEvery(tests)(6),
);

const nameMaker = (prefix, delimiter, suffix) => `${prefix}${delimiter}${suffix}`

// can only call once, with arguments: nameMaker('ant', '-', 'man');
console.log(nameMaker('ant', '-', 'man'));

const curriedNameMaker = fp.curry(nameMaker);

// currying creates a structure such that all arities of the original
// when passed to a function, return a similar function accepting 1 or
// more arguments, until all original curried arguments are provided.
// curry(f(x, y, z)) thus means:
//   f(x) => f(y, z)
//   f(x, y) => f(z)

console.log(curriedNameMaker('ant', '-', 'man'));
console.log(curriedNameMaker('ant')('-')('man'));
console.log(curriedNameMaker('ant', '-')('man'));
console.log(curriedNameMaker('ant')('-', 'man'));

// providing the first 2 args will yield a function that takes only one argument
const antPrefixer = curriedNameMaker('ant', '-');
console.log(antPrefixer('woman'));
console.log(antPrefixer('master'));

// Partial applications of functions are not like curries in that
// they do not produce variable-argument form.  They instead are much
// like creating a closure, as they yield a function that simply takes
// the remaining argument.  As a result, they are very good for creating
// functional-programming functions that can be used in compose, etc. that
// take in a single argument.

const megaMaker = ld.partial(nameMaker, 'mega', '_');
console.log(megaMaker('man'));

const equivalentMegaMaker = suffix => nameMaker('mega', '_', suffix);
console.log(equivalentMegaMaker('cat'));

// Unlike curries, the partial application doesn't result in an n-arity function
const antPrefix = ld.partial(nameMaker, 'ant');
console.log(antPrefix('-', 'man'));
// following won't work
// console.log(antPrefix('-')('man'))

const dashMaker = prefix => ld.partial(nameMaker, prefix, '-');
console.log(dashMaker('ant')('man'));
console.log(dashMaker('ant', 'man')); // doesn't work because not curried

const curriedDashMaker = fp.curry((prefix, suffix) => nameMaker(prefix, '-', suffix));
console.log(curriedDashMaker('ant')('woman'));
console.log(curriedDashMaker('ant', 'woman'));

// either of these can be used to create functions that can be composed, e.g.

const upperCase = x => x.toUpperCase();
const prefixer = ld.curry(ld.partial(nameMaker));
// ld.partial creates (x, y) => f(z)
// ld.curry curries this.

const spiderify = fp.compose(
    upperCase,
    prefixer('spider', '-'),
);

console.log(spiderify('man'));
console.log(spiderify('web'));

const exclaim = x => `${x}!`;

const importantSpiderify = fp.compose(
    exclaim,
    upperCase,
    prefixer('spider', '-'),
    // spiderify,
);

console.log(importantSpiderify('woman'));

const programaticSpiderify = suffix => `${nameMaker('spider', '-', suffix).toUpperCase()}!`;
console.log(programaticSpiderify('woman'));
