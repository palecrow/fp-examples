const prop = require('lodash/fp/prop');
const compose = require('lodash/fp/compose');
const allPass = require('lodash/fp/allPass');
const filter = require('lodash/fp/filter');
const uniq = require('lodash/fp/uniq');
const sort = require('lodash/fp/sortBy');
const curry = require('lodash/fp/curry');
const map = require('lodash/fp/map');
const mapValues = require('lodash/fp/mapValues');
const sortBy = require('lodash/sortBy');

const resource = {
    path: '/user/info/data',
    permissions: [
        { user: 'matt', isActive: true, type: 'WRITE' },
        { user: 'matt', isActive: false, type: 'DELETE' },
        { user: 'darren', isActive: true, type: 'WRITE' },
        { user: 'darren', isActive: true, type: 'CREATE' },
        { user: 'terry', isActive: true, type: 'READ' },
        { user: 'joe', isActive: true, type: 'READ' },
        { user: 'terry', isActive: true, type: 'DELETE' },
        { user: 'terry', isActive: true, type: 'CREATE' },
        { user: 'matt', isActive: true, type: 'CREATE' },
    ]
};

// FP-way

// The contexts for each function is small
//   no 'permissions' to confuse with 'permission' as above
// The functions are reusable because they're so atomic/small
//   isActive and anyOfTypes are probably useful elsewhere
// It's pretty easy to follow the compose chains
//   all the logic is in one place, not spread around

const isActive = prop('isActive');
const anyOfTypes = curry((types, permission) => types.includes(permission.type));
// The following will also work, but as we learned before currying is easy to
// use in compose().
// const anyOfTypes = types => permission => types.includes(permission.type);

const usersByTypes = (types, permissions) => compose(
    sortBy,
    uniq,
    map('user'),
    filter(allPass([
        isActive,
        anyOfTypes(types),
    ])),
)(permissions);

const fpResourcePermissionSummary = permissions => ({
    createOrWrite: usersByTypes(['CREATE', 'WRITE'], permissions),
    delete: usersByTypes(['DELETE'], permissions),
});

console.log(fpResourcePermissionSummary(resource.permissions));


// Variation: declarative.

// Declare the relationship between the key and the data, and
// provide a function to expand the two given a dataset.

const summaryMap = {
    createOrWrite: ['CREATE', 'WRITE'],
    delete: ['DELETE'],
};

const declarativeResourcePermissionSummary = (permissions, mapping) =>
  mapValues(x => usersByTypes(x, permissions))(mapping);

console.log(declarativeResourcePermissionSummary(resource.permissions, summaryMap));
